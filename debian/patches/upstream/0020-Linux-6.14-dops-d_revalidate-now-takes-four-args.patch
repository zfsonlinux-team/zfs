From 7ef6b70e960a7cc504242952699057f0ee616449 Mon Sep 17 00:00:00 2001
From: Rob Norris <robn@despairlabs.com>
Date: Wed, 5 Feb 2025 17:14:20 +1100
Subject: [PATCH 20/53] Linux 6.14: dops->d_revalidate now takes four args

This is a convenience for filesystems that need the inode of their
parent or their own name, as its often complicated to get that
information. We don't need those things, so this is just detecting which
prototype is expected and adjusting our callback to match.

Sponsored-by: https://despairlabs.com/sponsor/
Signed-off-by: Rob Norris <robn@despairlabs.com>
Reviewed-by: Alexander Motin <mav@FreeBSD.org>
Reviewed-by: Tony Hutter <hutter2@llnl.gov>
---
 config/kernel-automount.m4       | 41 ++++++++++++++++++++++++++++++--
 module/os/linux/zfs/zpl_ctldir.c |  6 +++++
 2 files changed, 45 insertions(+), 2 deletions(-)

diff --git a/config/kernel-automount.m4 b/config/kernel-automount.m4
index 52f1931b7..b5f1392d0 100644
--- a/config/kernel-automount.m4
+++ b/config/kernel-automount.m4
@@ -5,7 +5,7 @@ dnl # solution to handling automounts.  Prior to this cifs/nfs clients
 dnl # which required automount support would abuse the follow_link()
 dnl # operation on directories for this purpose.
 dnl #
-AC_DEFUN([ZFS_AC_KERNEL_SRC_AUTOMOUNT], [
+AC_DEFUN([ZFS_AC_KERNEL_SRC_D_AUTOMOUNT], [
 	ZFS_LINUX_TEST_SRC([dentry_operations_d_automount], [
 		#include <linux/dcache.h>
 		static struct vfsmount *d_automount(struct path *p) { return NULL; }
@@ -15,7 +15,7 @@ AC_DEFUN([ZFS_AC_KERNEL_SRC_AUTOMOUNT], [
 	])
 ])
 
-AC_DEFUN([ZFS_AC_KERNEL_AUTOMOUNT], [
+AC_DEFUN([ZFS_AC_KERNEL_D_AUTOMOUNT], [
 	AC_MSG_CHECKING([whether dops->d_automount() exists])
 	ZFS_LINUX_TEST_RESULT([dentry_operations_d_automount], [
 		AC_MSG_RESULT(yes)
@@ -23,3 +23,40 @@ AC_DEFUN([ZFS_AC_KERNEL_AUTOMOUNT], [
 		ZFS_LINUX_TEST_ERROR([dops->d_automount()])
 	])
 ])
+
+dnl #
+dnl # 6.14 API change
+dnl # dops->d_revalidate now has four args.
+dnl #
+AC_DEFUN([ZFS_AC_KERNEL_SRC_D_REVALIDATE_4ARGS], [
+	ZFS_LINUX_TEST_SRC([dentry_operations_d_revalidate_4args], [
+		#include <linux/dcache.h>
+		static int d_revalidate(struct inode *dir,
+		    const struct qstr *name, struct dentry *dentry,
+		    unsigned int fl) { return 0; }
+		struct dentry_operations dops __attribute__ ((unused)) = {
+			.d_revalidate = d_revalidate,
+		};
+	])
+])
+
+AC_DEFUN([ZFS_AC_KERNEL_D_REVALIDATE_4ARGS], [
+	AC_MSG_CHECKING([whether dops->d_revalidate() takes 4 args])
+	ZFS_LINUX_TEST_RESULT([dentry_operations_d_revalidate_4args], [
+		AC_MSG_RESULT(yes)
+		AC_DEFINE(HAVE_D_REVALIDATE_4ARGS, 1,
+		    [dops->d_revalidate() takes 4 args])
+	],[
+		AC_MSG_RESULT(no)
+	])
+])
+
+AC_DEFUN([ZFS_AC_KERNEL_SRC_AUTOMOUNT], [
+	ZFS_AC_KERNEL_SRC_D_AUTOMOUNT
+	ZFS_AC_KERNEL_SRC_D_REVALIDATE_4ARGS
+])
+
+AC_DEFUN([ZFS_AC_KERNEL_AUTOMOUNT], [
+	ZFS_AC_KERNEL_D_AUTOMOUNT
+	ZFS_AC_KERNEL_D_REVALIDATE_4ARGS
+])
diff --git a/module/os/linux/zfs/zpl_ctldir.c b/module/os/linux/zfs/zpl_ctldir.c
index fe64bc710..11438ab61 100644
--- a/module/os/linux/zfs/zpl_ctldir.c
+++ b/module/os/linux/zfs/zpl_ctldir.c
@@ -189,8 +189,14 @@ zpl_snapdir_automount(struct path *path)
  * as of the 3.18 kernel revaliding the mountpoint dentry will result in
  * the snapshot being immediately unmounted.
  */
+#ifdef HAVE_D_REVALIDATE_4ARGS
+static int
+zpl_snapdir_revalidate(struct inode *dir, const struct qstr *name,
+    struct dentry *dentry, unsigned int flags)
+#else
 static int
 zpl_snapdir_revalidate(struct dentry *dentry, unsigned int flags)
+#endif
 {
 	return (!!dentry->d_inode);
 }
-- 
2.39.5

